import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  base_url = 'http://localhost:80/PHP-Api';

  constructor(private http: HttpClient) { }

  getInfo() {
    return this.http.get(`${this.base_url}/getInfo.php`);
  }

  getInfectedByRegion() {
    return this.http.get(`${this.base_url}/getInfectedByRegion.php`);
  }
}
