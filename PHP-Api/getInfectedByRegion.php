<?php

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

    require_once('api_covid.php');

    if($_SERVER['REQUEST_METHOD'] === 'GET')
        echo json_encode($content['infectedByRegion']);
    else 
        throw new \Exception('405 Request Method is not Allowed');
