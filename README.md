## VERSÕES

### PHP 7.2
### Angular 7

## PASSOS PARA A EXECUÇÃO DO PROJETO

Php Api com Angular

npm install -g @angular/cli@7.* => Para instalar a última versão do angular 7

git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/projeto-php-api-angular.git

Mova o diretório PHP-Api para o servidor apache

Dentro de Angular-App: 

npm install => instalar as dependências
ng serve => executar a aplicação angular