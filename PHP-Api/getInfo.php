<?php

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

    require_once('api_covid.php');

    if($_SERVER['REQUEST_METHOD'] === 'GET') {

        $date_time = new \DateTime($content['lastUpdatedAtApify']);

        $date_time = $date_time->setTimeZone(new \DateTimeZone('America/Fortaleza'));
        
        $date_time = $date_time->format('d-m-Y') . ' ' . $date_time->format('H:m');

        $content   = [$date_time, $content['recovered'], $content['infected'], 
            $content['deceased']];
        
        echo json_encode($content);
    
    }else
        throw new \Exception('405 Request Method is not Allowed');
