import { IndexService } from './../../services/index.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';

declare const $;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  @ViewChild('tableInfectedByRegion') table;

  data_table: any;
  dtOption: any = {};

  last_updated: any;
  recovered: any;
  infected: any;
  deceased: any;

  infectedByRegion: any[];

  states: any[]          = [];
  counts: any[]          = [];
  backgroundColor: any[] = [];

  constructor(private indexService: IndexService) { }

  ngOnInit() {

    this.dataTableOption();

    this.getInfo();

    this.getInfectedByRegion();

    this.wait();
  }

  getInfo() {

    this.indexService.getInfo().subscribe(
      (response: any) => {
        this.last_updated = response[0];
        this.recovered    = response[1];
        this.infected     = response[2];
        this.deceased     = response[3];
      },

      error => {
        console.log(error);
      }
    )
  }

  getInfectedByRegion() {

    this.indexService.getInfectedByRegion().subscribe(
      (response: any) => {
        this.infectedByRegion = response;
      },

      error => {
        console.log(error);
      }
    )
  }

  wait() {
    setTimeout(() => {

      this.data_table = $(this.table.nativeElement);
      this.data_table.DataTable(this.dtOption);

      this.chartInfectedByRegion();
      this.chartBrasil();
    }, 2500);
  }

  dataTableOption() {

    this.dtOption = {
      pageLength: 5,
      lengthChange: false,
      info: false,
      language: {
        url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Portuguese-Brasil.json'
      }
    };
  }

  chartBrasil() {

    new Chart('chartBrasil', {
      type: 'doughnut',

      data: {
          labels: ["Recuperados", "Infectados", "Falecidos"],

          datasets: [{
              data: [this.recovered, this.infected, this.deceased],
              backgroundColor: ['#1cc88a', '#e74a3b', '#5a5c69'],
          }],
      },

      options: {
          maintainAspectRatio: false,

          tooltips: {
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
          },

          legend: {
              display: false
          },

          cutoutPercentage: 80,
      },
    });
  }

  chartInfectedByRegion() {

    this.infectedByRegion.forEach((content) => {
      this.states.push(content.state);
      this.counts.push(content.count);
      this.backgroundColor.push('#' + Math.floor(Math.random()*16777215).toString(16));
    });

    new Chart('chartState', {
      type: 'doughnut',

      data: {

          labels: this.states,

          datasets: [{
              data: this.counts,
              backgroundColor: this.backgroundColor
          }],
      },

      options: {
          maintainAspectRatio: false,

          tooltips: {
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              caretPadding: 10,
          },

          legend: {
              display: false
          },

          cutoutPercentage: 80,
      },
    });
  }
}
